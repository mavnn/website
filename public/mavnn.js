$(document).ready(function() {
    function chev(panel, chev) {
        $(panel).on('show.bs.collapse', function () {
            $(chev)
                .addClass("glyphicon-chevron-down")
                .removeClass("glyphicon-chevron-right");
        });

        $(panel).on('hide.bs.collapse', function () {
            $(chev)
                .addClass("glyphicon-chevron-right")
                .removeClass("glyphicon-chevron-down");
        });
    };

    chev('#panel1', '#chev1');
    chev('#panel2', '#chev2');
    chev('#panel3', '#chev3');
    chev('#panel4', '#chev4');
});
